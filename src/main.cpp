#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

#include <WiFi.h>
#include <WiFiClient.h>

#define LED 1
#define THUMB 39
#define INDEX 35
#define SAMPLES 5
#define INDEX_THRESH 60000
#define THUMB_THRESH 30000

int count = 0;
int counter = 0;
int mask_counter = 0;
const float VCC = 5.0;

float flexRAvg_T = 0.0f;
const float R_DIV_T = 51000.0;

float flexRAvg_I = 0.0f;
const float R_DIV_I = 51000.0;

uint8_t THUMB_MASK = 0;
uint8_t INDEX_MASK = 0;
bool prev_I = false;
bool prev_T = false;
bool peakI = false;
bool peakT = false;

float x = 0.0f;
float y = 0.0f;
float z = 0.0f;

// const char *SSID = "NETGEAR06";
// const char *PASS = "heavyviolin215";

const char *SSID = "AirPort";
const char *PASS = "nishantshah";

// const char* udpAddress = "10.0.1.5";
const int port = 4444;

boolean connected = false;
WiFiClient client;
WiFiServer server(port);

Adafruit_BNO055 bno = Adafruit_BNO055(55);

//wifi event handler
void WiFiEvent(WiFiEvent_t event){
    switch(event) {
        case SYSTEM_EVENT_STA_GOT_IP:
        //When connected set 
        Serial.print("WiFi connected! IP address: ");
        Serial.println(WiFi.localIP());
        connected = true;
        server.begin();
        break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        connected = false;
        break;
    }
}

void connectToWiFi(){
    // Serial.println("Connecting to WiFi network: " + String(SSID));
    
    // delete old config
    WiFi.disconnect(true);
    //register event handler
    WiFi.onEvent(WiFiEvent);
    
    //Initiate connection
    WiFi.begin(SSID, PASS);
    
    // Serial.println("Waiting for WIFI connection...");
}

void setup() {
    // put your setup code here, to run once:
    Serial.begin(115200);

    delay(1000);

    Serial.println("Initializing IMU.");
    /* Initialise the sensor */
    if(!bno.begin()) {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
        while(1);
    }

    bno.setExtCrystalUse(true);    
    Serial.println("IMU initialized.");

    connectToWiFi();
    // WiFi.begin(ssid, pass);
}

void printIMUData(WiFiClient c) {
    /* Get a new sensor event */ 
    
    // imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);    
    
    // /* Display the floating point data */
    // c.print("X: ");
    // c.print(gyro.x());
    // c.print(" Y: ");
    // c.print(gyro.y());
    // c.print(" Z: ");
    // c.print(gyro.z());
    // c.println("");
    
    // /* Display the floating point data */
    if (counter % SAMPLES == 0) {
        sensors_event_t event; 
        bno.getEvent(&event);
        imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
        x = euler.x();
        y = euler.y();
        z = euler.z();
    }
}

void printFlexSensor(WiFiClient c) {
    int flexADCT = analogRead(THUMB);
    int flexADCI = analogRead(INDEX);
    delay(20);
    float flexV_T = flexADCT * VCC / 4095.0;
    float flexR_T = R_DIV_I * (VCC / flexV_T - 1.0);

    float flexV_I = flexADCI * VCC / 4095.0;
    float flexR_I = R_DIV_T * (VCC / flexV_I - 1.0);

    if (counter % SAMPLES == 0) {
        flexRAvg_T = flexRAvg_T / SAMPLES;
        flexRAvg_I = flexRAvg_I / SAMPLES;
        if (flexRAvg_T > THUMB_THRESH) {
            THUMB_MASK |= (1 << mask_counter);
        } else {
            THUMB_MASK &= ~(1 << mask_counter);
        }

        if (flexRAvg_I > INDEX_THRESH) {
            INDEX_MASK |= (1 << mask_counter);
        } else {
            INDEX_MASK &= ~(1 << mask_counter);
        }
        peakT = !prev_T && (THUMB_MASK == 0b11111);
        peakI = !prev_I && (INDEX_MASK == 0b11111);
        Serial.printf("%d,%d\n", peakT, peakI);
        prev_T = (THUMB_MASK == 0b11111);
        prev_I = (INDEX_MASK == 0b11111);
        // Serial.printf("%f,%f\n", flexRAvg_1, flexRAvg_2);
        flexRAvg_T = 0.0f;
        flexRAvg_I = 0.0f;
        mask_counter++;
        if (mask_counter > 4) {
            mask_counter = 0;
        }
    }
    flexRAvg_T += flexR_T;
    flexRAvg_I += flexR_I;
}

void loop() {
    // put your main code here, to run repeatedly:
    
    WiFiClient c = server.available();
    if (c) {
        Serial.println("Client");
        while(c.connected()) {
            printIMUData(c);
            printFlexSensor(c);
            if (counter % SAMPLES == 0) {
                c.printf("%f,%f,%f,%d,%d:%d", x, y, z, peakT, peakI, counter);
            }
            counter++;    
        }
    }
    delay(10000 / SAMPLES);
}